CREATE DATABASE IF NOT EXISTS offersdtb;
use offersdtb;

CREATE TABLE IF NOT EXISTS Products(
        id VARCHAR(36),
        name VARCHAR(155),
        description VARCHAR(255),
        PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS Offers(
        id VARCHAR(36),
        price INTEGER,
        items_in_stock INTEGER,
        product_id VARCHAR(36),
        PRIMARY KEY (id)
);
