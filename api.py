"""Application for creating products and getting their offers"""
import os
from functools import wraps
import requests
from flask import Flask, request, make_response
from flask_apscheduler import APScheduler
import pymysql.cursors

app = Flask(__name__)

# Load the environment variables from the .env file
REFRESH_TOKEN = os.getenv('REFRESH_TOKEN')
OFFERS_URL = os.getenv('OFFERS_URL')
API_TOKEN = os.getenv('API_TOKEN')

# Limit for MySQL rows to return
MYSQL_LIMIT = 100

# How often to update offers database
SCHEDULER_INTERVAL = 60

REQUEST_TIMEOUT = 10

# Load latest ACCESS_TOKEN from previous session
with open("token", "r", encoding="utf-8") as token_file:
    os.environ['ACCESS_TOKEN'] = token_file.readline()


# Setup the database connection configuration
mysql_configuration = {
    'user': os.getenv('MYSQL_USER'),
    'password': os.getenv('MYSQL_PASSWORD'),
    'port': int(os.getenv('MYSQL_PORT')),
    'host': os.getenv('MYSQL_HOST'),
    'database': os.getenv('MYSQL_DATABASE')
}


def check_api_token(func):
    """Check if API token from request headers is correct"""
    @wraps(func)
    def handler(**kwargs):
        with app.app_context():
            token = request.headers.get('Token')
            if token != API_TOKEN:
                return make_response(), 401
            return func(**kwargs)
    return handler


def check_service_token(func):
    """Check and update offers service access token using the refresh token"""
    @wraps(func)
    def handler(**kwargs):
        url = f"{OFFERS_URL}/api/v1/auth"
        headers = {'Bearer': REFRESH_TOKEN}
        res = requests.post(url=url, headers=headers, timeout=REQUEST_TIMEOUT)

        if "access_token" in res.text:
            # pylint: disable=no-member
            app.logger.debug("New token:")
            app.logger.debug(res.text)

            access_token = res.json()["access_token"]
            os.environ['ACCESS_TOKEN'] = access_token

            # Save current token to the cache file to use it between the sessions
            # To avoid "Cannot generate access token because another is valid" condition
            with open("token", "w", encoding="utf-8") as file:
                file.write(access_token)

        return func(**kwargs)

    return handler


@check_service_token
def update_product_offers(product_id):
    """Update and return the latest offers of the product with a given uuid"""
    conn = pymysql.connect(**mysql_configuration)
    url = f"{OFFERS_URL}/api/v1/products/{product_id}/offers"
    headers = {'Bearer': os.environ['ACCESS_TOKEN']}
    res = requests.get(url=url, headers=headers, timeout=REQUEST_TIMEOUT)

    if "detail" in res.text:
        # pylint: disable=no-member
        app.logger.debug(res.text)
        # Something is wrong, see the details
        return res.json()

    offers = res.json()
    cur = conn.cursor()

    for offer in offers:
        price = offer["price"]
        stock = offer["items_in_stock"]

        query = f"""INSERT INTO Offers
        (id, price, items_in_stock, product_id)
        VALUES {offer["id"], price, stock, product_id}
        ON DUPLICATE KEY UPDATE price={price}, items_in_stock={stock};"""

        cur.execute(query)
        conn.commit()

    return res.json()


@app.get("/api/products")
@check_api_token
@check_service_token
def get_products():
    """Get info about all available products (up to set limit)"""
    conn = pymysql.connect(**mysql_configuration, cursorclass=pymysql.cursors.DictCursor)
    cur = conn.cursor()

    query = f"""SELECT Products.id, Products.name, Products.description
        FROM Products LIMIT {MYSQL_LIMIT};"""

    cur.execute(query)
    products = cur.fetchall()

    return make_response(products or {}), 200


@app.post("/api/products")
@check_api_token
@check_service_token
def add_product():
    """Adds new product to the database"""
    conn = pymysql.connect(**mysql_configuration)
    data = request.json

    url = f"{OFFERS_URL}/api/v1/products/register"
    headers = {'Bearer': os.environ['ACCESS_TOKEN']}
    res = requests.post(json=data, url=url, headers=headers, timeout=REQUEST_TIMEOUT)

    if "detail" in res.text:
        # Something is wrong, see the details
        return make_response(res.json()), 409

    cur = conn.cursor()

    product_id = data["id"]

    query = f"""INSERT INTO Products (id, name, description)
        VALUES {product_id, data["name"], data["description"]};"""

    cur.execute(query)
    conn.commit()

    # Add / update offers for the product in the database
    update_product_offers(product_id=product_id)

    return make_response(), 201


@app.patch("/api/products/<product_id>")
@check_api_token
@check_service_token
def update_product(product_id):
    """Update the product with the given id in the database
    :param product_id: UUID of the product
    """
    conn = pymysql.connect(**mysql_configuration)
    data = request.json

    new_name = data.get('name')
    new_description = data.get('description')

    if not new_name and not new_description:
        # Nothing to change
        return make_response(), 204

    cur = conn.cursor()
    update_query = ""

    if new_name and new_description:
        update_query = f"name='{new_name}', description='{new_description}'"
    elif new_name and not new_description:
        update_query = f"name='{new_name}'"
    elif not new_name and new_description:
        update_query = f"description='{new_description}'"

    query = f"""UPDATE Products SET {update_query}
        WHERE id='{product_id}';"""

    cur.execute(query)
    conn.commit()

    return make_response(), 200


@app.delete("/api/products/<product_id>")
@check_api_token
@check_service_token
def delete_product(product_id):
    """Delete the product with the given id from the database
    :param product_id: UUID of the product
    """
    conn = pymysql.connect(**mysql_configuration)

    cur = conn.cursor()

    query = f"""DELETE FROM Products, Offers
        USING Products INNER JOIN Offers
        WHERE Products.id=Offers.product_id
        AND Products.id='{product_id}'"""

    cur.execute(query)
    conn.commit()

    return make_response(), 204


@app.get("/api/offers")
@app.get("/api/offers/<product_id>")
@check_api_token
@check_service_token
def get_offers(product_id=None):
    """Get offers of the product with the given uuid
    :param product_id: uuid of the product
    """
    conn = pymysql.connect(**mysql_configuration,
                           cursorclass=pymysql.cursors.DictCursor)
    cur = conn.cursor()

    query = """SELECT Offers.id, Offers.price, Offers.items_in_stock, Offers.product_id
        FROM Offers JOIN Products
        ON Offers.product_id=Products.id"""
    if product_id:
        query += f" WHERE Products.id='{product_id}'"
    query += f" LIMIT {MYSQL_LIMIT};"

    cur.execute(query)
    offers = cur.fetchall()
    return make_response(offers or {}), 200


@check_service_token
def update_all_offers():
    """Update offers for all products in the database"""
    # pylint: disable=no-member
    app.logger.info("Update called")
    conn = pymysql.connect(**mysql_configuration, cursorclass=pymysql.cursors.SSCursor)
    cur = conn.cursor()
    query = "SELECT GROUP_CONCAT(id) FROM Products"
    cur.execute(query)
    products = cur.fetchone()

    if products[0] is not None:
        for product_id in products[0].split(","):
            update_product_offers(product_id=product_id)
    with app.app_context():
        return make_response(), 204


@app.put("/api/offers/<product_id>")
@check_api_token
@check_service_token
def update_offers(product_id):
    """Get latest offers of the product with the given uuid
    :param product_id: UUID of the product
    """
    update_product_offers(product_id=product_id)
    return make_response(), 204


# Run the scheduler, which will periodically call the update_all_offers function
scheduler = APScheduler()
scheduler.init_app(app)
scheduler.start()
INTERVAL_TASK_ID = 'update-all-offers'

scheduler.add_job(id=INTERVAL_TASK_ID,
                  func=update_all_offers,
                  trigger='interval',
                  seconds=SCHEDULER_INTERVAL)


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
