"""Tests for the Offers Obtainer application"""
import os
from pytest import fixture
import pymysql.cursors


SPICE_HARVESTER = {
    "id": "3fa85f64-5717-4562-b3fc-2c963f66af45",
    "name": "Spice Harvester",
    "description": "Spice Harvester obtains the Spice."
}

SAND_WORM = {
    "id": "3fa85f64-5717-4562-b3fc-2c963f66af45",
    "name": "Sand Worm",
    "description": "Spice Harvester obtains the Spice."
}

headers = {'Bearer': os.getenv('ACCESS_TOKEN')}

mysql_configuration = {
    'user': 'root',
    'password': 'root',
    'port': 3306,
    'cursorclass': pymysql.cursors.DictCursor
}


@fixture
def session():
    """Creates the database table"""
    conn = pymysql.connect(**mysql_configuration)
    cur = conn.cursor()

    query = """DROP DATABASE IF EXISTS testoffersdtb"""
    cur.execute(query)

    query = """CREATE DATABASE testoffersdtb"""
    cur.execute(query)

    query = """USE testoffersdtb"""
    cur.execute(query)

    query = """DROP TABLE IF EXISTS TestProducts;"""
    cur.execute(query)

    query = """CREATE TABLE TestProducts
        (id varchar(36), name varchar(155), description varchar(255));"""

    cur.execute(query)

    yield cur

    query = """DROP TABLE TestProducts;"""
    cur.execute(query)

    query = """DROP DATABASE testoffersdtb;"""
    cur.execute(query)

    conn.commit()

    conn.close()


def test_empty_table(session):
    """Test if the table is empty"""
    # pylint: disable=redefined-outer-name
    query = "SELECT * FROM TestProducts;"
    session.execute(query)
    data = session.fetchall()
    assert len(data) == 0


def test_data(session):
    """Test inserting, updating and checking data"""
    # pylint: disable=redefined-outer-name
    conn = pymysql.connect(**mysql_configuration, database='testoffersdtb')
    query = """INSERT INTO TestProducts (id, name, description)
    VALUES ('3fa85f64-5717-4562-b3fc-2c963f66af45',
    'Spice Harvester',
    'Spice Harvester obtains the Spice.');"""

    session.execute(query)
    conn.commit()

    query = "SELECT * FROM TestProducts;"
    session.execute(query)
    data = session.fetchall()
    assert len(data) > 0
    assert data[0] == SPICE_HARVESTER

    query = """UPDATE TestProducts SET name='Sand Worm'
        WHERE name='Spice Harvester';"""
    session.execute(query)
    conn.commit()

    query = "SELECT * FROM TestProducts;"
    session.execute(query)

    data = session.fetchall()

    assert len(data) > 0
    assert data[0] == SAND_WORM


def test_delete_data(session):
    """Test deleting data"""
    # pylint: disable=redefined-outer-name
    conn = pymysql.connect(**mysql_configuration, database='testoffersdtb')
    query = "DELETE FROM TestProducts;"
    session.execute(query)
    conn.commit()

    query = "SELECT * FROM TestProducts;"
    session.execute(query)
    data = session.fetchall()
    assert len(data) == 0
